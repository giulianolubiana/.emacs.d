(use-package rtags
  :ensure t
  :config
  (setq rtags-completions-enabled t)
  (setq rtags-autostart-diagnostics t)
  (rtags-enable-standard-keybindings)
  (setq rtags-path
        (expand-file-name "~/opensource/rtags/build")))

(rtags-start-process-unless-running)
(provide 'setup-rtags)
