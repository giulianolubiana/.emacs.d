# README #

It containes a compositional emacs setting file, mostlly based on C++.

In the folder custom you can find different setting files; their are all indipendent. If you want to use one you just have to write inside init.el the corresponding require command.

Es:
```elisp
(require 'setup-general)
```

### How do I get set up? ###
To set it up you just have to clone the repo and open emacs. It could be necessary to run

```
package-refresh-contents
```

A bit more of setting up must be done if you want to use rtags and cmake-ide.

To install rtags refer to [Rtags QuickSetUp](https://github.com/Andersbakken/rtags#tldr-quickstart). 
Once you have installed Rtags and built the db tag you have to set in [setup-rtags.el](https://bitbucket.org/giulianolubiana/.emacs.d/src/master/custom/setup-rtags.el) the path to the rtags executables.

To use cmake-ide you just have to set your working directory insede [setup-general.el](https://bitbucket.org/giulianolubiana/.emacs.d/src/master/custom/setup-general.el). 
